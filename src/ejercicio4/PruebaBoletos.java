/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio4;
import java.util.Scanner;

/**
 *
 * @author Usuario
 */
class Entradas{
    String identificador;
    String nombre;
    String zona;
    double precio;
    int contador =0;
    public Entradas(){};
    public Entradas(String nombre) {
        this.nombre = nombre;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String identificarZona(String arreglo[],int opc){
        switch(opc){
            case 1:
                zona = "Principal";
                break;
            case 2:
                zona = "Palco";
                break;
            case 3: 
                zona = "Centro";
                break;
            case 4: 
                zona ="Lateral";
                break;
        }
        return zona;
    }
    public String Espacio(String arreglo[],int opc,String[]arreglo1){
        String bandera= "null";
        identificarZona(arreglo,opc);
        String puesto=" ";
        for (int i = contador; i < arreglo.length; i++) {
          if(!bandera.equals(arreglo[i])){
                identificador = (i+1)+zona;
                contador++;
                arreglo[i]= identificador;
                arreglo1[i]= toString();
                return identificador;
            }
      }
        return "No existe espacio en la localidad ";
    }
    public void BuscarEntrada(String arreglo[],String valor,String arreglo1[]){
        for (int i = 0; i < arreglo.length; i++) {
            if(valor.equals(arreglo[i])){
                System.out.println(arreglo1[i]);
            }
        }
    }

    @Override
    public String toString() {
        return "Nombre:  "+nombre+"\nId de la entrada: "+identificador+"\nZona: " + zona  ;
    }
    
}
class Normales extends Entradas{
    public Normales(){};
    public double precio(String arreglo[],int opc){
        super.identificarZona(arreglo, opc);
        if("Principal".equals(zona))
            super.precio= 25; 
        if ("Palco".equals(zona)) 
            super.precio = 70;
        if ("Centro".equals(zona)) 
            super.precio = 20;
        if("Lateral".equals(zona))
            super.precio = 15.50;
        return precio;
    }
}
class Reducidas extends Normales{
    @Override
    public double precio(String arreglo[],int opc){
        precio = (super.precio(arreglo, opc)-(super.precio(arreglo, opc)*0.15));
        return precio;
    }
}
class Abonado extends Entradas{
    public Abonado(){};
    public double precio(String arreglo[],int opc){
        super.identificarZona(arreglo, opc);
        if("Principal".equals(zona))
            super.precio= 17.5; 
        if ("Palco".equals(zona)) 
            super.precio = 40;
        if ("Centro".equals(zona)) 
            super.precio = 14;
        if("Lateral".equals(zona))
            super.precio = 10;
        return precio;
    }
}
public class PruebaBoletos {
    static int opc,opcion=0,elegir=0,valor;
    static String identificador;
    static double sum1=0,sum2=0,sum3=0,sum4=0;
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        Entradas entrada1 = new Entradas();
        Normales normal1= new Normales();
        Reducidas reducidas1= new Reducidas();
        Abonado abonado1= new Abonado();
        String principal []= new String [2];
        String aux1 []= new String [2];
        String palco []= new String [40];
        String aux2 []= new String [40];
        String central []= new String [400];
        String aux3 []= new String [400];
        String lateral []= new String [100];
        String aux4 []= new String [100];
        while(opcion!=4){
            System.out.println("1.Vender Entrada\n2.Consulta de entrada\n3.Informe de zona\n4.Salir");opcion= teclado.nextInt();
            switch(opcion){
                case 1: 
                    System.out.println("Elija la localidad en la que desea su boleto \n1.Principal\n2.Palco\n3.Central\n4.Lateral");opc= teclado.nextInt();
                    System.out.println("Ingrese el nombre: ");entrada1.setNombre(teclado.next());
                    switch(opc){
                    case 1:
                        entrada1.identificarZona(principal,opc);
                        entrada1.Espacio(principal, opc,aux1);
                        System.out.println(entrada1);
                        System.out.println("Ingrese el tipo de entrada que desee: \n1.Normal\n2.Reducidas\n3.Abonado");elegir = teclado.nextInt();
                        if(elegir ==1){
                            System.out.println("Precio: "+normal1.precio(principal, opc));
                            sum1+= normal1.precio(principal, opc);
                        }
                        if(elegir==2){
                            System.out.format("Precio: %.2f\n",reducidas1.precio(principal, opc));
                            sum1+= reducidas1.precio(principal, opc);
                        }
                        if(elegir==3){
                            System.out.println("Precio: "+abonado1.precio(principal, opc)); 
                            sum1+= abonado1.precio(principal, opc);
                        }
                        break;
                    case 2:
                        entrada1.identificarZona(palco,opc);
                        entrada1.Espacio(palco, opc,aux2);
                        System.out.println(entrada1.toString());
                        System.out.println("Ingrese el tipo de entrada que desee: \n1.Normal\n2.Reducidas\n3.Abonado");elegir = teclado.nextInt();
                         if(elegir ==1){
                            System.out.println("Precio: "+normal1.precio(palco, opc));
                            sum2+= normal1.precio(palco, opc);
                        }
                        if(elegir==2){
                            System.out.format("Precio: %.2f\n",reducidas1.precio(palco, opc));
                            sum2+= reducidas1.precio(palco, opc);
                        }
                        if(elegir==3){
                            System.out.println("Precio: "+abonado1.precio(palco, opc)); 
                            sum2+= abonado1.precio(palco, opc);
                        }
                        break;
                    case 3:
                        entrada1.identificarZona(central,opc);
                        entrada1.Espacio(central, opc,aux3);
                        System.out.println(entrada1.toString());
                        System.out.println("Ingrese el tipo de entrada que desee: \n1.Normal\n2.Reducidas\n3.Abonado");elegir = teclado.nextInt();
                        if(elegir ==1){
                            System.out.println("Precio: "+normal1.precio(central, opc));
                            sum3+= normal1.precio(central, opc);
                        }
                        if(elegir==2){
                            System.out.format("Precio: %.2f\n",reducidas1.precio(central, opc));
                            sum3+= reducidas1.precio(central, opc);
                        }
                        if(elegir==3){
                            System.out.println("Precio: "+abonado1.precio(central, opc)); 
                            sum3+= abonado1.precio(central, opc);
                        }
                        break;
                    case 4:
                        entrada1.identificarZona(lateral,opc);
                        entrada1.Espacio(lateral, opc,aux4);
                        System.out.println(entrada1.toString());
                        System.out.println("Ingrese el tipo de entrada que desee: \n1.Normal\n2.Reducidas\n3.Abonado");elegir = teclado.nextInt();
                         if(elegir ==1){
                            System.out.println("Precio: "+normal1.precio(lateral, opc));
                            sum4+= normal1.precio(lateral, opc);
                        }
                        if(elegir==2){
                            System.out.format("Precio: %.2f\n",reducidas1.precio(lateral, opc));
                            sum4+= reducidas1.precio(lateral, opc);
                        }
                        if(elegir==3){
                            System.out.println("Precio: "+abonado1.precio(lateral, opc)); 
                            sum4+= abonado1.precio(lateral, opc);
                        }
                        break;      
                    }
                    break;
                case 2:
                    System.out.print("Ingrese el identificador a consultar :");identificador= teclado.next();
                    System.out.println("Ingrese la categoria en la que desea buscar: \n1.Principal\n2.Palco\n3.Central\n4.Lateral"); valor = teclado.nextInt();
                    if(valor==1)
                        entrada1.BuscarEntrada(principal, identificador, aux1);
                    if(valor==2)
                        entrada1.BuscarEntrada(palco, identificador, aux2);
                    if(valor==3)
                        entrada1.BuscarEntrada(central, identificador, aux3);
                    if(valor==4)
                         entrada1.BuscarEntrada(lateral, identificador, aux4);
                    break;
                case 3:
                    System.out.println("Ingrese la categoria en la que desea buscar: \n1.Principal\n2.Palco\n3.Central\n4.Lateral"); valor = teclado.nextInt();
                    if(valor==1)
                        System.out.println("Recaudacion total de la zona Principal es : "+sum1);
                    if(valor==2)
                        System.out.println("Recaudacion total de la zona Palco es : "+sum2);
                    if(valor==3)
                        System.out.println("Recaudacion total de la zona Central es: "+sum3);
                    if(valor==4)
                        System.out.println("Recaudacion total de la zona Lateral es: "+sum4);
                    break;
                }
        }
    }
}
